# Decidim-catraca-embarcado

| Integrantes                   |
|------------------------|
| Abner Filipe           |
| Allecsander Lélis      |
| Ana Aparecida          |
| Bianca Sofia     |
| Daniel Primo           |
| Júlio César            |
| Lorrany Souza          |
| Lorrayne Alves        |
| Lucas Pimentel         |
| Nicolas                |
| Marina Costa           |
| Pedro Campos           |
| Rafael Leão            |
| Ramires                |
| Thiago Galletti        |
| Thiago França Vale Oliveira |
| Vitor Magalhães        |

## Como usar

O software embarcado foi desenvolvido na arduino IDE para ser utilizado em uma ESP32 DEVKIT V1. Os outros componentes eletrônicos utilizados são um LCD I2C e dois relays conectados nas portas 23 e 25. O software utiliza a conexão bluetooth da placa para realizar a comunicação com o app afim de receber dados no formato UTF8 e tratá-los da devida forma.
