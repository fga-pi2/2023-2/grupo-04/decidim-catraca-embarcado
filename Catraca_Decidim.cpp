
// ============================================================================
// --- Bibliotecas Auxiliares ---
#include <Wire.h>
#include <LiquidCrystal_I2C.h>
#include <BLEDevice.h>
#include <BLEServer.h>
#include <BLEUtils.h>
#include <BLE2902.h>

// --- Variáveis utilizadas para conexão BLE ---
BLECharacteristic *pCharacteristic;
BLEServer *pServer;
BLEService *pService;
bool deviceConnected = false;
int txValue = 0;

int dcaux = 0; // Variável auxiliar para controle de dispositivo conectado
bool isOpened = false; // Variável para saber o status da cancela
 
LiquidCrystal_I2C lcd(0x27, 16, 2);

// --- Declaração de funções de controle da catraca ---
void abrirTrava(std::string nome);
void fecharTrava();

// --- Definição do UUID da conexão BLE ---
#define SERVICE_UUID "6E400001-B5A3-F393-E0A9-E50E24DCCA9E"
#define CHARACTERISTIC_UUID_RX "6E400002-B5A3-F393-E0A9-E50E24DCCA9E"
#define CHARACTERISTIC_UUID_TX "6E400003-B5A3-F393-E0A9-E50E24DCCA9E"

// Classe responsável por controlar as conexões e desconexões da placa
class ServerCallbacks: public BLEServerCallbacks {
  void onConnect(BLEServer* pServer){
    deviceConnected = true;
    Serial.println("Dispositivo conectado");
    BLEDevice::startAdvertising();
  };

  void onDisconnect(BLEServer* pServer){
    deviceConnected = false;
    dcaux = 0;
    Serial.println("Dispositivo desconectado");
    fecharTrava();
  }
};

// Classe responsável por escutar e tratar os sinais recebidos via BLE
class MyCallbacks: public BLECharacteristicCallbacks {
  void onWrite(BLECharacteristic *pCharacteristic) {
    Serial.println("aqui 0");
    std::string rxValue = pCharacteristic->getValue();
    Serial.println("aqui 1");
    if (rxValue.length() > 0) {
      Serial.println("======START=RECEIVE======");
      Serial.print("Received Value: ");
      std::string name = "";
      std::string value = "";
      for (int i = 0; i < rxValue.length() - 1; i++) {
        name = name + rxValue[i];
      }
      value = rxValue[rxValue.length() - 1];
      Serial.println(name.c_str());
      if(value == "1"){
        abrirTrava(name);
      }else if(value == "0"){
        fecharTrava();
      }
      Serial.println();
    }
  }
};

// ============================================================================
// --- Mapeamento de Hardware ---
#define   relay1   23                    //pino para controle do relé 1
#define   relay2   25                    //pino para controle do relé 2

// ============================================================================

// --- Configurações Iniciais ---
void setup() 
{
  lcd.init();
  lcd.backlight();
  Serial.begin(115200);                  //inicializa Serial em 115200 baud rate
  Serial.println("Serial iniciada!");
  pinMode(relay1, OUTPUT);               //configura saída para relé 1
  pinMode(relay2, OUTPUT);               //configura saída para relé 2

  BLEDevice::init("Catraca_Decidim");

  pServer = BLEDevice::createServer();
  pServer->setCallbacks(new ServerCallbacks);

  pService = pServer->createService(SERVICE_UUID);

  pCharacteristic = pService->createCharacteristic(CHARACTERISTIC_UUID_TX, BLECharacteristic::PROPERTY_NOTIFY);

  pCharacteristic->addDescriptor(new BLE2902());

  BLECharacteristic *pCharacteristic = pService->createCharacteristic(CHARACTERISTIC_UUID_RX, BLECharacteristic::PROPERTY_WRITE);

  pCharacteristic->setCallbacks(new MyCallbacks());
  
  pService->start();

  pServer->getAdvertising()->start();
  Serial.println("Dispositivo pronto para conexão");
  dcaux = 1;

}


// ============================================================================
// --- Loop Infinito ---
void loop() 
{

} 
//end loop

// Função que abre a catraca caso esteja fechada
void abrirTrava(std::string nome) {
  /* Liga o motor */
  
  lcd.init();
  lcd.print(nome.c_str());
  lcd.setCursor(0, 1);
  lcd.print("Liberado!");
  if(!isOpened){
    digitalWrite(relay1, HIGH);
    delay(550);
    digitalWrite(relay1, LOW);
    digitalWrite(relay2, LOW);
  }
  delay(2500);
  Serial.println("aqui 5");
  isOpened = true;
  lcd.init();
  
  /* Desliga o motor */
}

// Função que fecha a catraca caso esteja aberta
void fecharTrava() {
  /* Liga o motor */
  
  lcd.init();
  lcd.print("Sem cadastro");
  lcd.setCursor(0, 1);
  lcd.print("Acesso Negado!");
  if(isOpened){
    digitalWrite(relay2, HIGH);
    delay(550);
    digitalWrite(relay1, LOW);
    digitalWrite(relay2, LOW);
  }
  delay(2500);
  isOpened = false;
  lcd.init();
}

/* ========================================================   
======================================================== */
/* --- Final do Programa --- */